package com.haider.com.listviewandroid;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class UserDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int user_id = getIntent().getExtras().getInt("user_id",0);

        DatabaseHandler db = new DatabaseHandler(this);
        User user = db.getUser(user_id);
        db.close();

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(user.getName());

        TextView hometown = (TextView)findViewById(R.id.hometown);
        hometown.setText(user.getHometown());


    }

}
