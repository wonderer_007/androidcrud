package com.haider.com.listviewandroid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class newUser extends AppCompatActivity {

    Context context;
    EditText name;
    EditText hometown;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        name = (EditText) findViewById(R.id.name);
        hometown = (EditText) findViewById(R.id.hometown);
        context = this;


        save = (Button) findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().length() != 0 && hometown.getText().toString().length() != 0) {

                    DatabaseHandler db = new DatabaseHandler(context);
                    db.createUser(new User(name.getText().toString(), hometown.getText().toString()));
                    db.close();

                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);

                }
                else
                    Toast.makeText(context, "Please fill all fields", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
