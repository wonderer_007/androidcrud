package com.haider.com.listviewandroid;

/**
 * Created by haiderali on 30/03/2016.
 */
public class User {

      int id;
     String name;
     String hometown;

    public User(String name, String hometown) {
        this.name = name;
        this.hometown = hometown;
    }

    public User(int id, String name, String hometown) {
        this.id = id;
        this.name = name;
        this.hometown = hometown;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }
}
