package com.haider.com.listviewandroid;

/**
 * Created by haiderali on 30/03/2016.
 */
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

    private SQLiteDatabase database;
    private DatabaseHandler dbHelper;
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "users",
            TABLE_USER = "users", KEY_ID = "id", KEY_NAME = "name",
            KEY_HOMETOWN = "hometown";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = " CREATE TABLE " + TABLE_USER + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NAME + " TEXT, "
                + KEY_HOMETOWN + " TEXT ) ";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_USER);
        onCreate(db);
    }

    public void createUser(User user) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, user.getName());
        values.put(KEY_HOMETOWN, user.getHometown());

        db.insert(TABLE_USER, null, values);
        db.close();

    }


    public ArrayList<User> getAllUsers() {

        ArrayList<User> users = new ArrayList<User>();
        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.rawQuery(" SELECT * FROM " + TABLE_USER, null);

        if (cursor.moveToFirst()) {
            do {
                User user = new User(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2));

                users.add(user);

            } while (cursor.moveToNext());

        }
        return users;
    }
    public User getUser(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER, new String[]{KEY_ID, KEY_NAME, KEY_HOMETOWN},
                KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null,
                null, null);

        if (cursor != null)
            cursor.moveToFirst();

        User user = new User(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));

        db.close();
        cursor.close();

        return user;
    }

}